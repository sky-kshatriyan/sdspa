/*
Dedicated file to include all React components that map to an AEM component
*/
 
// src/components/MappedComponents.js
 
require('./page/Page');
require('./text/Text');
require('./image/Image');
require('./list/List');