import React, { Component } from 'react';
import { Link } from "react-router-dom";
import Logo from '../../logo_usbank.png';
import { withRouter } from 'react-router';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './Header.scss';
require('../../utils/Icons');


class Header extends Component {

    get homeLink() {
        let currLocation;
        currLocation = this.props.location.pathname;
        currLocation = currLocation.substr(0, currLocation.length - 5);

        if (this.props.navigationRoot && currLocation !== this.props.navigationRoot) {
            return (<Link className="Header-action" to={this.props.navigationRoot + ".html"}>
                <FontAwesomeIcon icon="chevron-left" />
            </Link>);
        }
        return null;
    }

    render() {
        return (
            <header className="Header">
                <div className="Header-wrapper">
                    <div className="Header-title"><img src={Logo} alt="logo" /></div>
                    <div className="Header-sdtools">
                        <ul>
                          <li><a href="#home">My Accounts</a></li>
                          <li><a href="#news">Transfers</a></li>
                          <li><a href="#contact">Bill payments</a></li>
                          <li><a href="#about">Send Money</a></li>
                          <li><a href="#about">Deposits</a></li>
                          <li><a href="#about">Customer Service</a></li>
                        </ul>
                    </div>
                    <div className="Header-tools">
                        { this.homeLink }
                   </div>
                </div>
            </header>
        );
    }
}

export default withRouter(Header); 
